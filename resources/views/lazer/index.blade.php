@extends('layout.principal')

@section('conteudo')

<div class="container">
<h1>Lazer</h1>

  <table id="portoes" class="table table-striped table-bordered table-hover">

  <tr>
    <td><strong>Som</strong> </td>
      <td> 
        <a href="{{action('LazerController@ligarsom')}}">        
          <button type="button" name="r01" class="btn btn-success">On</button>
        </a>      
    </td>
    <td>
      <a href="{{action('LazerController@desligarsom')}}"> 
        <button type="button" class="btn btn-danger">Off</button>
      </a>
    </td>
  </tr>

  </table>
</div>

@stop

