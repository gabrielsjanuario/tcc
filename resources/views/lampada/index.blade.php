@extends('layout.principal')

@section('conteudo')

<form method="post">
<div class="container">
<h1>Iluminação</h1>

  <table class="table table-striped table-bordered table-hover">
  <tr>
    <td><strong>Sala</strong> </td>
      <td> 
        <a href="{{action('LampadaController@ligarsala')}}">        
          <button type="button" name="r01" class="btn btn-success">On</button>
        </a>      
    </td>
    <td>
      <a href="{{action('LampadaController@desligarsala')}}"> 
        <button type="button" class="btn btn-danger">Off</button>
      </a>
    </td>
  </tr>

  <td><strong>Quarto 1</strong> </td>
      <td> 
        <a href="{{action('LampadaController@ligarquarto1')}}">        
          <button type="button" name="r01" class="btn btn-success">On</button>
        </a>      
    </td>
    <td>
      <a href="{{action('LampadaController@desligarquarto1')}}"> 
        <button type="button" class="btn btn-danger">Off</button>
      </a>
    </td>
  </tr>

  <td><strong>Quarto 2</strong> </td>
      <td> 
        <a href="{{action('LampadaController@ligarquarto2')}}">        
          <button type="button" name="r01" class="btn btn-success">On</button>
        </a>      
    </td>
    <td>
      <a href="{{action('LampadaController@desligarquarto2')}}"> 
        <button type="button" class="btn btn-danger">Off</button>
      </a>
    </td>
  </tr>

  <!-- <td><strong>Garagem</strong> </td>
      <td> 
        <a href="{{action('LampadaController@ligargaragem')}}">        
          <button type="button" name="r01" class="btn btn-success">On</button>
        </a>      
    </td>
    <td>
      <a href="{{action('LampadaController@desligargaragem')}}"> 
        <button type="button" class="btn btn-danger">Off</button>
      </a>
    </td>
  </tr> -->

  <td><strong>Cozinha</strong> </td>
      <td> 
        <a href="{{action('LampadaController@ligarcozinha')}}">        
          <button type="button" name="r01" class="btn btn-success">On</button>
        </a>      
    </td>
    <td>
      <a href="{{action('LampadaController@desligarcozinha')}}"> 
        <button type="button" class="btn btn-danger">Off</button>
      </a>
    </td>
  </tr>


  </table>
</div>
</form>
@stop



