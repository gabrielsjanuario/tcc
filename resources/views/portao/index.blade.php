
@extends('layout.principal')

@section('conteudo')


<form method="post">
<div class="container">
<h1>Portões</h1>

  <table id="portoes" class="table table-striped table-bordered table-hover">
    <tr>
      <td><strong>Portão principal</strong> </td>
        <td> 
<a href="{{action('PortaoController@ligar')}}">        
       <button type="button" name="r01" class="btn btn-success">On</button>
</a>      
</td>
      <td>
      <a href="{{action('PortaoController@desligar')}}"> 
            <button type="button" class="btn btn-danger">Off</button>
      </a>
      </td>
    </tr>

  </table>
</div>
</form>
@stop

