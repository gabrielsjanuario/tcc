<html>
	<head>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

		<link href="/css/app.css" rel="stylesheet">
		<link href="/css/custom.css" rel="stylesheet">
		<link href="/css/bootstrap.css" rel="stylesheet">
		<script href="/js/bootstrap.min.js" rel="stylesheet"></script>


		<title>Gerenciador de Residencia</title>
	</head>
	<body>

		<nav class="navbar navbar-default">

		<div class="container-fluid">
		<a href="/home" > <img id="LOGO" src="/images/home_icon-icons.com_73532.png"> </a>   


		</div>
		</nav>

		@yield('conteudo')

		<footer class="footer">
		<p></p>
		</footer>

	</body>
</html>