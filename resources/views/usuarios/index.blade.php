@extends('layout.principal')

@section('conteudo')

<div class="container">
    <h1>Cadastrar usuário</h1>

    <form id="form-usuario" action="/home" >
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
        <div class="form-group">
            <label>Nome</label>
                <input name="nome" class="form-control">
        </div>
        <div class="form-group">
            <label>E-mail</label>
            <input name="email" class="form-control">
        </div>

        <div class="form-group">
            <label>Senha</label>
            <input name="senha" class="form-control" type="number"/>
        </div>
            <button type="submit" class="btn btn-primary btn-block">Salvar</button>
    </form>

</div>

@stop