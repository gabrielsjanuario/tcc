@extends('layout.principal')

@section('conteudo')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"rel="stylesheet">

<div class="container">
<h1>Configurações</h1>

  <table id="config" class="table table-striped  table-hover">
    <tr>
        <td> 
            <span class="menu-item lightblue"> <i class="material-icons">vpn_lock</i></span>
        </td>

        <td>
            <a href="/usuarios/listar"><strong>Usuários</strong></a> 
        </td>
    </tr>
    <tr>
        <td> 
            <span class="menu-item lightblue"> <i class="material-icons">vpn_key</i> </span>
        </td>

        <td>
            <a href="/"><strong>Sair</strong> </a>
        </td>
    </tr>
  </table>

</div>

@stop