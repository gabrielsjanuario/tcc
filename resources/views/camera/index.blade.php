@extends('layout.principal')

@section('conteudo')

<div class="container">
<h1>Segurança</h1>

  <table id="cameras" class="table ">

    <tr>
      <td><strong>Alarme</strong> </td>
        <td> 
        <a href="{{action('CamerasController@ligaralarme')}}">        
          <button type="button" name="r01" class="btn btn-success">On</button>
        </a>    
      </td>
      <td>
      <a href="{{action('CamerasController@desligaralarme')}}"> 
        <button type="button" class="btn btn-danger">Off</button>
      </a>
      </td>
    </tr>

    <tr>
      <td><strong>Câmera</strong> </td>

        <td> 
          <a href="">        
            <button type="button" name="r01" class="btn btn-success">Ver</button>
          </a>      
      </td>
    </tr>


  </table>
</div>

@stop

