<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index.index');
});

Route::get('/home', function () {
    return view('welcome');
});

Route::get('/lampadas' , 'LampadaController@index');
Route::get('/usuarios' , 'UsuariosController@index');
Route::get('/usuarios/listar' , 'UsuariosController@listar');
Route::get('/configuracoes' , 'ConfiguracoesController@index');
Route::get('/portao' , 'PortaoController@index');
Route::get('/cameras' , 'CamerasController@index');
Route::get('/lazer' , 'LazerController@index');

Route::get('/portao/ligar','PortaoController@ligar');
Route::get('/portao/desligar','PortaoController@desligar');

//ILUMINACAO
Route::get('/lampada/ligarsala','LampadaController@ligarsala');
Route::get('/lampada/desligarsala','LampadaController@desligarsala');

Route::get('/lampada/ligarquarto1','LampadaController@ligarquarto1');
Route::get('/lampada/desligarquarto1','LampadaController@desligarquarto1');

Route::get('/lampada/ligargaragem','LampadaController@ligargaragem');
Route::get('/lampada/desligargaragem','LampadaController@desligargaragem');

Route::get('/lampada/ligarcozinha','LampadaController@ligarcozinha');
Route::get('/lampada/desligarcozinha','LampadaController@desligarcozinha');

Route::get('/lampada/ligarquarto2','LampadaController@ligarquarto2');
Route::get('/lampada/desligarquarto2','LampadaController@desligarquarto2');

Route::get('/lazer/ligarsom', 'LazerController@ligarsom');
Route::get('/lazer/desligarsom', 'LazerController@desligarsom');

Route::get('/cameras/ligaralarme', 'CamerasController@ligaralarme');
Route::get('/cameras/desligaralarme', 'CamerasController@desligaralarme');


Route::get('/login','LoginController@login');






