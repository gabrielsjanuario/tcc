<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CamerasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('camera.index');
    }

    public function ligaralarme()     
    {
        echo shell_exec("curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F transistorname=transistor1 -F state=high http://192.168.43.48:9000/relay/transistor_on_off");
        return redirect('/cameras');
     
    }
    public function desligaralarme()     
    {        
        echo shell_exec("curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F transistorname=transistor1 -F state=low http://192.168.43.48:9000/relay/transistor_on_off");     
        return redirect('/cameras');

    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
