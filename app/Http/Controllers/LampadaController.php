<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LampadaController extends Controller
{
    
    public function index()
    {
        return view('lampada.index');
    }

    public function ligarsala()     
    {        
        echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay8 http://192.168.43.48:9000/relay/toogle_relay");
        return redirect('/lampadas');
     
    }
    public function desligarsala()     
    {        
        echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay8 http://192.168.43.48:9000/relay/toogle_relay");     
        return redirect('/lampadas');

    }

    public function ligarquarto1()     
    {        
        echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay10 http://192.168.43.48:9000/relay/toogle_relay");
        return redirect('/lampadas');
     
    }
    public function desligarquarto1()     
    {        
        echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay10 http://192.168.43.48:9000/relay/toogle_relay");     
        return redirect('/lampadas');

    }

    public function ligarquarto2()     
    {        
        echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay9 http://192.168.43.48:9000/relay/toogle_relay");
        return redirect('/lampadas');
     
    }
    public function desligarquarto2()     
    {        
        echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay9 http://192.168.43.48:9000/relay/toogle_relay");     
        return redirect('/lampadas');

    }

    public function ligarcozinha()     
    {        
        echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay7 http://192.168.43.48:9000/relay/toogle_relay");
        return redirect('/lampadas');
     
    }
    public function desligarcozinha()     
    {        
        echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay7 http://192.168.43.48:9000/relay/toogle_relay");     
        return redirect('/lampadas');

    }

    // public function ligargaragem()     
    // {        
    //     echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay1 http://192.168.43.48:9000/relay/toogle_relay");
    //     return redirect('/lampadas');
     
    // }
    // public function desligargaragem()     
    // {        
    //     echo shell_exec("sudo curl -X POST -F token=2e52d3eb834e09f30509fcf4837478f207e71f59 -F relayname=relay1 http://192.168.43.48:9000/relay/toogle_relay");     
    //     return redirect('/lampadas');

    // }
}

